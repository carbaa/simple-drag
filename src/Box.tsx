import React, { RefObject } from "react";

export default function Box({
  forwardedRef,
  ...props
}: {
  forwardedRef?: RefObject<any>;
} & React.HTMLAttributes<HTMLDivElement>) {
  return <div {...props} ref={forwardedRef}></div>;
}
