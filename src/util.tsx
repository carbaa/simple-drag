export const convertStrToNum = (str: string) => Number(str.replace("px", ""));

export const getTrueBoundaries = (element: HTMLElement) => {
  const rect = element.getBoundingClientRect();
  const style = getComputedStyle(element);

  const left =
    rect.left +
    convertStrToNum(style.borderLeftWidth) +
    convertStrToNum(style.paddingLeft);
  const right =
    rect.right -
    convertStrToNum(style.borderRightWidth) -
    convertStrToNum(style.paddingRight);
  const top =
    rect.top +
    convertStrToNum(style.borderTopWidth) +
    convertStrToNum(style.paddingTop);
  const bottom =
    rect.bottom -
    convertStrToNum(style.borderBottomWidth) -
    convertStrToNum(style.paddingBottom);

  return { left, right, top, bottom };
};
