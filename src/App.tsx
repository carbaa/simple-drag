import React from "react";
import "./App.css";
import Draggable from "./Draggable";
import Box from "./Box";

function App() {
  return (
    <div className="App">
      <Draggable>
        <Box className="box" />
      </Draggable>
    </div>
  );
}

export default App;
