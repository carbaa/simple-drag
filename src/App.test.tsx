import React from "react";
import { render, screen } from "@testing-library/react";

import Draggable from "./Draggable";
import Box from "./Box";

describe("essential rendering test", () => {
  beforeEach(() => {
    render(
      <div data-testid="container">
        <Draggable>
          <Box data-testid="box" className="box" />
        </Draggable>
      </div>
    );
  });

  test("see if the container is rendered", () => {
    const container = screen.getByTestId("container");
    expect(container).toBeInTheDocument();
  });

  test("see if the box is rendered", () => {
    const box = screen.getByTestId("box");
    expect(box).toBeInTheDocument();
  });
});
