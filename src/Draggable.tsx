import React from "react";
import { getTrueBoundaries } from "./util";

export default function Draggable({ children }: { children: JSX.Element }) {
  const callbackRef = (element: HTMLElement) => {
    if (element) {
      // Get parent. However, it'd be weird to drag the root element.
      const parent = element.parentElement ? element.parentElement : element;
      const parentRect = getTrueBoundaries(parent);
      const childRect = getTrueBoundaries(element);

      const minTranslateX = parentRect.left - childRect.left;
      const maxTranslateX = parentRect.right - childRect.right;
      const minTranslateY = parentRect.top - childRect.top;
      const maxTranslateY = parentRect.bottom - childRect.bottom;

      let mousedown = false;
      let startX = 0;
      let startY = 0;
      let translateX = 0;
      let translateY = 0;
      let lastTranslateX = 0;
      let lastTranslateY = 0;

      const onMouseDown = (e: MouseEvent) => {
        mousedown = true;
        startX = e.clientX;
        startY = e.clientY;
      };

      const onTouchStart = (e: TouchEvent) => {
        mousedown = true;
        startX = e.targetTouches[0].clientX;
        startY = e.targetTouches[0].clientY;
      };

      const move = (e: MouseEvent | Touch) => {
        translateX = lastTranslateX + e.clientX - startX;
        translateY = lastTranslateY + e.clientY - startY;

        // Bind to the boundaries
        translateX = translateX < minTranslateX ? minTranslateX : translateX;
        translateX = translateX > maxTranslateX ? maxTranslateX : translateX;
        translateY = translateY < minTranslateY ? minTranslateY : translateY;
        translateY = translateY > maxTranslateY ? maxTranslateY : translateY;

        element.style.transform = `translate(${translateX}px, ${translateY}px)`;
      };

      const onMouseMove = (e: MouseEvent) => {
        if (mousedown && element) move(e);
      };

      const onTouchMove = (e: TouchEvent) => {
        if (mousedown && element) move(e.targetTouches[0]);
      };

      const cleanUp = () => {
        mousedown = false;
        lastTranslateX = translateX;
        lastTranslateY = translateY;
      };

      const onMouseUp = (e: MouseEvent) => {
        cleanUp();
      };

      const onTouchEnd = (e: TouchEvent) => {
        cleanUp();
      };

      element.addEventListener("touchstart", onTouchStart);
      element.addEventListener("touchmove", onTouchMove);
      element.addEventListener("touchend", onTouchEnd);

      element.addEventListener("mousedown", onMouseDown);
      element.addEventListener("mousemove", onMouseMove);
      element.addEventListener("mouseup", onMouseUp);
    }
  };

  return React.cloneElement(children, { forwardedRef: callbackRef });
}
